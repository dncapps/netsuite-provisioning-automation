#!/usr/local/bin/python3

import os
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--no-sandbox")
driver = webdriver.Chrome(chrome_options=chrome_options)
driver.get("https://system.netsuite.com")

print('##### Supply Credentials')
userNameEntry = driver.find_element_by_id('userName')
userNameEntry.send_keys(os.environ['NSUSERNAME'])

passwordEntry = driver.find_element_by_id('password')
passwordEntry.send_keys(os.environ['NSPASSWORD'])

submitButtonPage1 = driver.find_element_by_id('submitButton')
submitButtonPage1.submit()
#####

print('##### Supply Security Answer')
securityAnswer = driver.find_element_by_name('answer')
securityAnswer.send_keys(os.environ['NSSECURITY'])

submitButtonPage2 = driver.find_element_by_name('submitter')
submitButtonPage2.submit()
#####

print('##### Use Custom Record Entry URL and ID')
driver.get("https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=27&id=1350&e=T")
#####

print('##### Click and Save')
provisionCheck = driver.find_element_by_id('custrecord_ra_provisioning_set_chbx_fs')
provisionCheck.click()
submitButtonPage3 = driver.find_element_by_id('btn_multibutton_submitter')
submitButtonPage3.submit()

time.sleep(10)

print('##### Use Custom Record Entry URL and ID')
driver.get("https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=27&id=1350&e=T")
#####

provisionURL = driver.find_element_by_id('custrecord_ra_workstation_installurl_val')
print(provisionURL.get_attribute("href"))

#print('##### Use Custom Record List URL')
#driver.get("https://system.netsuite.com/app/common/custom/custrecordentrylist.nl?rectype=27")
######
#
#print('##### Use Custom Records URL')
#driver.get("https://system.netsuite.com/app/common/custom/custrecords.nl?whence=")
######
#
#print('##### Navigate ahead')
#navigNext = driver.find_element_by_class_name('navig-next')
#navigNext.click()
######

#print(driver.page_source)
driver.close()
